# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule KeysCasePlugin do
  use Plugin
  require Logger

  @spec transform(map, map) :: [map]
  def transform(dict, options) do
    if not MapUtils.all_required_keys?(options, ["case", "mode"]) do
      [dict]
    else
      keys = Map.keys(dict)

      List.foldl(keys, dict, fn old_key, acc ->
	new_key =
	  if options["case"] == "lower",
	    do: String.downcase(old_key),
	    else: String.upcase(old_key)

	if old_key != new_key do
	  value = acc[old_key]

	  acc
	  |> KeysAddPlugin.transform(%{"keys" => %{new_key => value}, "mode" => options["mode"]})
	  |> Enum.at(0)
	  |> KeysDeletePlugin.transform(%{"keys" => [old_key]})
	else
	  [acc]
	end
      end)
    end
  end

end
