# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule RecordSplitPlugin do
  @moduledoc """
  Split a record in several records. Example:
  From
     [%{"k1" => "v1", "k2" => "word1,word2,word3"}]
  To:
     [%{ "k1" => "v1",
	 "k2" => "word1"},
       %{"k1" => "v1",
	 "k2" => "word2"},
       %{"k1" => "v1",
	 "k2" => "word3"}
  Using the rule
  [
      %{
	"action" => "ValueSplit",
	"key" => "k2",
	"pattern" => ","
      }
    ]
  """
  use Plugin
  require Logger

  def transform(dict, options) do
    if not MapUtils.all_required_keys?(options, ["key", "pattern"]) do
      [dict]
    else
      Logger.debug("options ='" <> inspect(options) <> "'")

      key = options["key"]
      pattern = options["pattern"]

      case Map.fetch(dict, key) do
	:error ->
	  Logger.error("Missing key '" <> key)
	 [dict]

	{:ok, value} ->
	  values = String.split(value, pattern)
	  Enum.map(values, &Map.put(dict, key, &1))
      end
      # case
    end
  end
end
