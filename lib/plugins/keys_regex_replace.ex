# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule KeysRegexReplacePlugin do
  use Plugin
  require Logger

  def transform(dict, options) do
    if not MapUtils.all_required_keys?(options, ["key_regex", "replace_with", "mode"]) do
      [dict]
    else
      replace_with = options["replace_with"]

      case RegexUtils.compile(options["key_regex"]) do
	{:error, error} ->
	  Logger.error("Error: " <> error <> " Wrong regex '" <> options["key_regex"] <> "'")
	  [dict]

	{:ok, regex} ->
	  candidate_keys = MapUtils.filter_keys_by_regex(dict, regex)

	  new_dict = List.foldl(candidate_keys, dict, fn old_key, acc ->
	    new_key = Regex.replace(regex, old_key, replace_with)

	    if old_key != new_key do
	      Logger.warn("Adding key: '" <> new_key <> "'")
	      Logger.warn("Removing key: '" <> old_key <> "'")
	      value = acc[old_key]

	      acc
	      |> KeysAddPlugin.transform(%{
		"keys" => %{new_key => value},
		"mode" => options["mode"]
					 })
	      |> Enum.at(0)
	      |> KeysDeletePlugin.transform(%{"keys" => [old_key]})
	      |> Enum.at(0)
	    else
	      acc
	    end
	  end)
	  [new_dict]
	  # foldl
      end

      # case
    end

  end
end
