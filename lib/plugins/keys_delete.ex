# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule KeysDeletePlugin do
  use Plugin
  require Logger

  @spec transform(map, map) :: [map]
  def transform(dict, options) do
    new_dict = if MapUtils.all_required_keys?(options, ["keys"]) do
      Logger.warn("deleting keys '" <> Enum.join(options["keys"], ",") <> "'")

      dict
      |> Map.drop(options["keys"])
      |> MapUtils.log("dropped keys " <> inspect(options["keys"]))
    else
      dict
    end
    [new_dict]
  end
end
