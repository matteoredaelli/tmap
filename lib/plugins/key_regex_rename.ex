# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule KeyRegexRenamePlugin do
  use Plugin
  require Logger

  @spec transform(map, map) :: [map]
  def transform(dict, options) do
    if not MapUtils.all_required_keys?(options, ["new_key", "key_regex", "mode"]) do
      [dict]
    else
      new_key = options["new_key"]
      candidate_keys = MapUtils.filter_keys_by_regex(dict, options["key_regex"])

      if Enum.count(candidate_keys) > 0 do
	old_key = List.first(candidate_keys)
	value = dict[old_key]

	dict
	|> KeysAddPlugin.transform(%{"keys" => %{new_key => value}, "mode" => options["mode"]})
	|> Enum.at(0)
	|> KeysDeletePlugin.transform(%{"keys" => [old_key]})
      else
	[dict]
      end
    end

  end

end
