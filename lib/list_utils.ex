defmodule ListUtils do
  require Logger
  def filter_by_regex(list, regex) when is_nil(list) or is_nil(regex), do: []

  def filter_by_regex(list, regex) when is_list(list) and is_binary(regex) do
    case RegexUtils.compile(regex) do
      {:ok, regex} ->
        filter_by_regex(list, regex)

      {:error, error} ->
        Logger.error("Wrong regex '" <> regex <> "': the error is: '" <> inspect(error))
        []
    end
  end

  def filter_by_regex(list, regex) when is_nil(regex) or (is_list(list) and length(list)) == 0,
    do: []

  def filter_by_regex(list, regex) when is_list(list) do
    Enum.filter(list, fn x -> Regex.match?(regex, x) end)
  end

  def intersection(list1, list2) do
    MapSet.intersection(Enum.into(list1, MapSet.new()), Enum.into(list2, MapSet.new()))
    |> MapSet.to_list()
  end
end
