defmodule RegexUtils do
  def compile(string) when is_binary(string) do
    Regex.compile(string, [:caseless])
  end
end
