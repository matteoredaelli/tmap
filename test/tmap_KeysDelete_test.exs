defmodule TMapTestKeysDelete do
  use ExUnit.Case, async: true
  doctest TMap

  # 1533042873,
  test "KeysDelete" do
    data = [%{"k1" => "v1", "k2" => "v2"}]
    rules = [%{"action" => "KeysDelete", "keys" => ["k2", "k3"]}]
    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1"}]
  end
end
