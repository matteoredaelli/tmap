defmodule TMapTestKeyAddIf do
  use ExUnit.Case, async: true

  doctest TMap

  test "KeyAddIf" do
    data = [%{"k1" => "v1"}]
    rules = [%{"action" => "KeyAddIf", "key" => "k2", "value" => "v2", "mode" => "no_overwrite"}]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1", "k2" => "v2"}]
  end

  test "KeyAddIf key_regex" do
    data = [%{"k1" => "v1"}]

    rules = [
      %{
	"action" => "KeyAddIf",
	"key" => "k2",
	"value" => "v2",
	"key_regex" => "^k.",
	"mode" => "no_overwrite"
      }
    ]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1", "k2" => "v2"}]
  end

  test "KeyAddIf key_regex ko" do
    data = [%{"k1" => "v1"}]

    rules = [
      %{
	"action" => "KeyAddIf",
	"key" => "k2",
	"value" => "v2",
	"key_regex" => "2$",
	"mode" => "no_overwrite"
      }
    ]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1"}]
  end

  test "KeyAddIf key_regex value_regex" do
    data = [%{"k1" => "v1"}]

    rules = [
      %{
	"action" => "KeyAddIf",
	"key" => "k2",
	"value" => "v2",
	"key_regex" => "^k.",
	"value_regex" => "^v.",
	"mode" => "no_overwrite"
      }
    ]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1", "k2" => "v2"}]
  end

  # 1533042873,
  test "KeyAddIf key_regex value_regex ko" do
    data = [%{"k1" => "v1"}]

    rules = [
      %{
	"action" => "KeyAddIf",
	"key" => "k2",
	"value" => "v2",
	"key_regex" => "^k.",
	"value_regex" => "2$",
	"mode" => "no_overwrite"
      }
    ]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1"}]
  end

  test "KeyAddIf key_regex value_regex FSL" do
    data = [%{"k1" => "Firehawk SZ 90 XL FSL"}]

    rules = [
      %{
	"action" => "KeyAddIf",
	"key" => "k2",
	"value" => "true",
	"key_regex" => "k1",
	"value_regex" => "(^|\s)(FL|FR|FP|FSL|MFS|ML|RPB)(\s|$)",
	"mode" => "no_overwrite"
      }
    ]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "Firehawk SZ 90 XL FSL", "k2" => "true"}]
  end
end
