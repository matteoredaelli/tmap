defmodule TMapTestKeysAdd do
  use ExUnit.Case, async: true
  doctest TMap

  # 1533042873,
  test "KeysAdd" do
    data = [%{"k1" => "v1"}]
    rules = [%{"action" => "KeysAdd", "keys" => %{"k2" => "v2"}, "mode" => "no_overwrite"}]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1", "k2" => "v2"}]
  end
end
