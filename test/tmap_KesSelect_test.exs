defmodule TMapTestKeysFilter do
  use ExUnit.Case, async: true
  doctest TMap

  test "keysSelect" do
    data = [%{"k1" => "v1", "k2" => "v2", "k3" => "v3"}]
    rules = [%{"action" => "KeysSelect", "keys" => ["k1", "k2"]}]
    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1", "k2" => "v2"}]

    rules = [%{"action" => "KeysSelect", "keys" => ["k1", "k4"]}]
    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1", "k4" => Nil}]
  end
end
