defmodule TMapTestKeysRegexReplace do
  use ExUnit.Case, async: true
  doctest TMap

  test "KeyRegexReplace" do
    data = [%{"k1" => "v1", "k1a" => "v2"}]

    rules = [
      %{
	"action" => "KeysRegexReplace",
	"replace_with" => "11",
	"key_regex" => "1$",
	"mode" => "no_overwrite"
      }
    ]

    assert TMap.apply_rules(data, rules) == [%{"k11" => "v1", "k1a" => "v2"}]
  end

  test "KeyRegexReplace spaces and other strange characters" do
    data = [%{~s( 'k1'   ) => "v1", ~s('"k 2"\n) => "v2"}]

    rules = [
      %{
	"action" => "KeysRegexReplace",
	"replace_with" => "",
	"key_regex" => ~s([ '"\t\n']+),
	"mode" => "no_overwrite"
      }
    ]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1", "k2" => "v2"}]
  end
end
