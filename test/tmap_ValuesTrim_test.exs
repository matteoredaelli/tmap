defmodule TMapTestValuesTrim do
  use ExUnit.Case, async: true
  doctest TMap

  test "ValuesTrim" do
    data = [%{"k1 " => " v1", " k2" => "v2 "}]
    rules = [%{"action" => "ValuesTrim"}]
    assert TMap.apply_rules(data, rules) == [%{"k1 " => "v1", " k2" => "v2"}]
  end
end
