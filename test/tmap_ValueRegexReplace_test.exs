defmodule TMapTestValueRegexReplace do
  use ExUnit.Case, async: true

  doctest TMap

  test "ValueRegexReplace" do
    data = [%{"k1" => "v1", "k2" => "v121"}]
    rules = [%{"action" => "ValueRegexReplace", "keys" => ["k1"], "replace_with" => "11", "value_regex" => "1$"}]
    assert TMap.apply_rules(data, rules) == [%{"k1" => "v11", "k2" => "v121"}]
  end

  test "ValueRegexReplaceYes" do
    data = [%{"k1" => "Sì", "k2" => "YES"}]

    rules = [
      %{"action" => "ValueRegexReplace", "keys" => ["k2"], "replace_with" => "TRUE", "value_regex" => "Sì|YES"}
    ]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "Sì", "k2" => "TRUE"}]
  end

  test "ValuesRegexReplace special characters" do
    data = [%{"k1" => "v1", "k2" => "v1 2"}]
    rules = [%{"action" => "ValueRegexReplace",  "keys" => ["k2"], "replace_with" => "", "value_regex" => ~s([ ]+)}]
    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1", "k2" => "v12"}]
  end
end
