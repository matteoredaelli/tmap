defmodule TMapTestValueRegexNamedCaptures do
  use ExUnit.Case, async: true
  doctest TMap

  test "ValueRegexNamedCaptures" do
    data = [%{"k1" => "v1", "k2" => "word1 this is a text"}]

    rules = [
      %{
	"action" => "ValueRegexNamedCaptures",
	"key" => "k2",
	"value_regex" => "^(?<word>[^ ]+) (?<text>.*)$"
      }
    ]

    assert TMap.apply_rules(data, rules) == [%{
	     "k1" => "v1",
	     "k2" => "word1 this is a text",
	     "word" => "word1",
	     "text" => "this is a text"
	   }]
  end
end
